terraform {
  required_version = ">= 0.12"

  required_providers {
    helm = ">= 1.0.0"
  }
}

locals {
  harbor_external_domain_core   = format("core-harbor-test.%s", var.external_domain)
  harbor_external_domain_notary = format("notary-harbor-test.%s", var.external_domain)
}

provider "helm" {
  kubernetes {
    config_path = "~/.kube/config.anagog"
  }
}


data "helm_repository" "harbor" {
  name = "harbor"
  url  = "https://helm.goharbor.io"
}

resource "random_string" "harbor_secret_key" {
  length  = 16
  upper   = true
  special = true
}


resource "helm_release" "harbor" {
  repository = data.helm_repository.harbor.name
  version    = var.harbor_helm_version
  chart      = "harbor"
  name       = "harbor-test"
  namespace  = var.k8s_namespace

  values = [
    <<-EOF
    expose:
      type: ingress
      ingress:
        hosts:
          core: ${local.harbor_external_domain_core}
          notary: ${local.harbor_external_domain_notary}
    externalURL: "https://${local.harbor_external_domain_core}"
    secretKey: ${random_string.harbor_secret_key.result}
    EOF
    ,
  ]

  lifecycle {
    ignore_changes = [keyring]
  }

  depends_on = [
    data.helm_repository.harbor,
    kubernetes_secret.harbor_docker_registry
  ]
}

resource "kubernetes_secret" "harbor_docker_registry" {

  metadata {
    name      = "jedai-read-only"
    namespace = var.k8s_namespace
  }

  data = {
    ".dockerconfigjson" = "${templatefile("${path.module}/docker_registry.json", {
      url      = local.harbor_external_domain_core
      username = "jedai-read-only"
      password = "Test1234"
    })}"
  }

  type = "kubernetes.io/dockerconfigjson"
}
