variable "k8s_namespace" {
  description = "K8S working namespace"
  default     = "env-a"
}

variable "external_domain" {
  description = "External Domain"
  default     = "anagog.com"
}
variable "harbor_helm_version" {
  description = "Harbor Helm chart version"
  default     = "1.3.1"
}
