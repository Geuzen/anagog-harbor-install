output "harbor_external_domains" {
  description = "Harbor External Domans"
  value = [
    local.harbor_external_domain_core,
    local.harbor_external_domain_notary
  ]
}
